using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDataContext: DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        
        public EfDataContext(DbContextOptions<EfDataContext> options): base(options)
        {
            
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // modelBuilder.Entity<Role>()
            //     .HasData(FakeDataFactory.Roles);
            // modelBuilder.Entity<Employee>()
            //     .HasData(FakeDataFactory.Employees);
            // modelBuilder.Entity<Preference>()
            //     .HasData(FakeDataFactory.Preferences);
            // modelBuilder.Entity<Customer>()
            //     .HasData(FakeDataFactory.Customers);
            // modelBuilder.Entity<CustomerPreference>()
            //     .HasData(FakeDataFactory.CustomerPreferences);
        }
    }
}