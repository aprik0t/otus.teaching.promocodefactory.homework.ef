using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>: IRepository<T> where T:BaseEntity
    {
        private readonly EfDataContext _db;
        private readonly DbSet<T> _data;
        
        public EfRepository(EfDataContext db)
        {
            _db = db;
            _data = db.Set<T>();
        }
        
        public async Task<T[]> GetAllAsync()
        { 
            return await _data.AsNoTracking().ToArrayAsync();
        }

        public Task<T[]> GetByIdsAsync(IEnumerable<Guid> ids)
        {
            return _data.Where(d => ids.Contains(d.Id)).AsNoTracking().ToArrayAsync();
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return _db.Set<T>().FindAsync(id).AsTask();
        }

        public Task CreateAsync(T entity)
        {
            _data.Add(entity);
            return _db.SaveChangesAsync();
        }

        public Task UpdateAsync(T entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
            return _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            _data.Remove(entity);
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> FilterAsync(Func<T, bool> expression)
        {
            return _data.Where(expression);
        }
    }
}