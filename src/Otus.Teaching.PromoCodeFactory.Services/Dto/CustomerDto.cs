﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Services.Dto
{
    public class CustomerDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceDto> Preferences { get; set; } = new();
        public List<PromoCodeDto> PromoCodes { get; set; } = new();
    }
}