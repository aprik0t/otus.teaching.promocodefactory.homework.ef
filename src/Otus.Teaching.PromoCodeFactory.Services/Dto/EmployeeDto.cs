﻿using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.Services.Dto
{
    public class EmployeeDto
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }

        public string Email { get; set; }

        public RoleItemDto Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}