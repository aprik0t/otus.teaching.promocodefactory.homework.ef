﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Services.Dto
{
    public class RoleItemDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
    }
}