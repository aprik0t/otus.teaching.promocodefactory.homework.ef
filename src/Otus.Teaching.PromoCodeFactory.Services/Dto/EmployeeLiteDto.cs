﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Services.Dto
{
    public class EmployeeLiteDto
    {
        public Guid Id { get; set; }
        
        public string FullName { get; set; }

        public string Email { get; set; }
    }
}