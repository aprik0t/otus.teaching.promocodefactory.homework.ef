using System;

namespace Otus.Teaching.PromoCodeFactory.Services.Dto
{
    public class PreferenceDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}