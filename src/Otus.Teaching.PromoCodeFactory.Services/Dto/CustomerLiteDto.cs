﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Services.Dto
{
    public class CustomerLiteDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}