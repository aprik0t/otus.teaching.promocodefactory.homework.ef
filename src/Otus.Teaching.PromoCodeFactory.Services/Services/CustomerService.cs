﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Dto;
using Otus.Teaching.PromoCodeFactory.Services.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.Services.Services
{
    public class CustomerService: ICustomerService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<CustomerPreference> _customerPreferencesRepository;
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IMapper _mapper;

        public CustomerService(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferencesRepository, 
            IRepository<CustomerPreference> customerPreferencesRepository, 
            IRepository<PromoCode> promoCodesRepository,
            IMapper mapper)
        {
            _customerRepository = customerRepository;
            _preferencesRepository = preferencesRepository;
            _customerPreferencesRepository = customerPreferencesRepository;
            _promoCodesRepository = promoCodesRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CustomerLiteDto>> LoadAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            return customers
                .Select(_mapper.Map<CustomerLiteDto>)
                .ToList();
        }

        public async Task<CustomerDto> GetByIdAsync(Guid id)
        {
            var customer = (await _customerRepository.GetByIdAsync(id));
            if (customer is null)
                throw new NotFoundException(typeof(Customer), id);

            var dto = _mapper.Map<CustomerDto>(customer);
            var customerPrefs = (await _customerPreferencesRepository
                .FilterAsync(p => p.CustomerId == customer.Id))
                .ToList();
            if (!customerPrefs.Any()) 
                return dto;
            var preferenceIds = customerPrefs.Select(cp => cp.PreferenceId);
            var preferences = await _preferencesRepository.GetByIdsAsync(preferenceIds);
            dto.Preferences = preferences.Select(_mapper.Map<PreferenceDto>).ToList();

            var promoCodes = await _promoCodesRepository.FilterAsync(p => p.CustomerId == id);
            dto.PromoCodes = promoCodes.Select(_mapper.Map<PromoCodeDto>).ToList();
            return dto;
        }

        public Task CreateAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer();
            _mapper.Map(request, customer);
            return _customerRepository.CreateAsync(customer);
        }

        public async Task UpdateAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                throw new NotFoundException(typeof(Customer), id);
            await DeletePreferencesFor(customer);
            
            _mapper.Map(request, customer);
            await _customerRepository.UpdateAsync(customer);
        }

        public async Task DeleteAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                throw new NotFoundException(typeof(Customer), id);
            
            await DeletePreferencesFor(customer);
            await _customerRepository.DeleteAsync(customer);
        }

        private async Task DeletePreferencesFor(Customer customer)
        {
            if (customer.CustomerPreferences is not null)
            {
                var customerPrefs = await _customerPreferencesRepository
                    .FilterAsync(p => p.CustomerId == customer.Id);
                foreach (var customerPref in customerPrefs)
                    await _customerPreferencesRepository.DeleteAsync(customerPref);
            }
        }
    }
}