using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Services.Dto;
using Otus.Teaching.PromoCodeFactory.Services.Request;

namespace Otus.Teaching.PromoCodeFactory.Services.Services
{
    public interface IPromoCodesService
    {
        Task<IEnumerable<PromoCodeDto>> LoadAsync();
        Task CreateAndGivePromocode(GivePromoCodeRequest request);
    }
}