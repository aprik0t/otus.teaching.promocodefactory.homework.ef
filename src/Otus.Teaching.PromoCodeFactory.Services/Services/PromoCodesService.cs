using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Dto;
using Otus.Teaching.PromoCodeFactory.Services.Exceptions;
using Otus.Teaching.PromoCodeFactory.Services.Request;

namespace Otus.Teaching.PromoCodeFactory.Services.Services
{
    public class PromoCodesService: IPromoCodesService
    {
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Employee> _employeesRepository;
        private readonly IRepository<CustomerPreference> _customerPreferencesRepository;
        private readonly IMapper _mapper;

        public PromoCodesService(IRepository<Customer> customersRepository, 
            IRepository<PromoCode> promoCodesRepository, 
            IRepository<Preference> preferencesRepository, 
            IRepository<Employee> employeesRepository, 
            IRepository<CustomerPreference> customerPreferencesRepository,
            IMapper mapper)
        {
            _customersRepository = customersRepository;
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _employeesRepository = employeesRepository;
            _mapper = mapper;
            _customerPreferencesRepository = customerPreferencesRepository;
        }

        public async Task<IEnumerable<PromoCodeDto>> LoadAsync()
        {
            var promoCodes = await _promoCodesRepository.GetAllAsync();
            return promoCodes.Select(_mapper.Map<PromoCodeDto>).ToList();
        }

        public async Task CreateAndGivePromocode(GivePromoCodeRequest request)
        {
            var customer = await FilterSingle(_customersRepository, request.CustomerEmail,
                c => c.Email.Equals(request.CustomerEmail));
            var preference = await FilterSingle(_preferencesRepository, request.Preference,
                c => c.Name.Equals(request.Preference));
            var partner = await FilterSingle(_employeesRepository, request.PartnerEmail,
                c => c.Email.Equals(request.PartnerEmail));

            var preferenceLink = (await _customerPreferencesRepository
                .FilterAsync(p => p.CustomerId == customer.Id && p.PreferenceId == preference.Id))
                .FirstOrDefault();
            if (preferenceLink == null)
                throw new NotFoundException($"У пользователя {customer.Id} нет предпочтений категории '{preference.Name}'");

            await _promoCodesRepository.CreateAsync(new PromoCode
            {
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = request.BeginDate,
                EndDate = request.EndDate,
                CustomerId = customer.Id,
                PreferenceId = preference.Id,
                PartnerManagerId = partner.Id,
                PartnerEmail = request.PartnerEmail,
                Id = Guid.NewGuid()
            });
        }

        private static async Task<T> FilterSingle<T>(IRepository<T> repository, string identity, Func<T, bool> filter) where T : BaseEntity
        {
            var items = (await repository.FilterAsync(filter)).ToList();
            if (!items.Any())
                throw new NotFoundException(typeof(T), identity);
            if (items.Count > 1)
                throw new TooManyResultsException(typeof(T));
            return items.First();
        }
    }
}