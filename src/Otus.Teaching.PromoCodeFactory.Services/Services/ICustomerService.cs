using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Services.Dto;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.Services.Services
{
    public interface ICustomerService
    {
        Task<IEnumerable<CustomerLiteDto>> LoadAsync();
        Task<CustomerDto> GetByIdAsync(Guid id);
        Task CreateAsync(CreateOrEditCustomerRequest request);
        Task UpdateAsync(Guid id,CreateOrEditCustomerRequest request);
        Task DeleteAsync(Guid id);
    }
}