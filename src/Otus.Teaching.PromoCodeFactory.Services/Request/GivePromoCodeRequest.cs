﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Services.Request
{
    public class GivePromoCodeRequest
    {
        public string ServiceInfo { get; set; }

        public string PartnerEmail { get; set; }

        public string PromoCode { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Preference { get; set; }
        
        public string CustomerEmail { get; set; }
    }
}