using System;

namespace Otus.Teaching.PromoCodeFactory.Services.Exceptions
{
    public class TooManyResultsException: Exception
    {
        public TooManyResultsException(Type type): base($"Слишком много результатов при поиске '{type.Name}'")
        {
            
        }
    }
}