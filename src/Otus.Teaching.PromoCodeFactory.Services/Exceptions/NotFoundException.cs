using System;

namespace Otus.Teaching.PromoCodeFactory.Services.Exceptions
{
    public class NotFoundException: Exception
    {
        public NotFoundException(Type type, Guid id): base($"{type.Name} with id '{id:D}' not found.")
        {
            
        }

        public NotFoundException(Type type, string identity) : base($"{type.Name} with identity '{identity}' not found.")
        {
            
        }

        public NotFoundException(string message) : base(message)
        {
            
        }
    }
}