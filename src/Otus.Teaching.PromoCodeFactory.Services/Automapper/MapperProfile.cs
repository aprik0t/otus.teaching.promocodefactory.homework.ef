using System.Linq;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Dto;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.Services.Automapper
{
    public class MapperProfile: Profile
    {
        public MapperProfile()
        {
            CreateMap<PromoCode, PromoCodeDto>();
            CreateMap<PromoCodeDto, PromoCode>();
            
            CreateMap<Customer, CustomerLiteDto>();
            CreateMap<CustomerLiteDto, Customer>();

            CreateMap<Customer, CustomerDto>();
            CreateMap<CustomerDto, Customer>();

            CreateMap<CreateOrEditCustomerRequest, Customer>()
                .ForMember(c => c.CustomerPreferences, c => c.MapFrom(
                    f => f.PreferenceIds.Select(p => new CustomerPreference{ PreferenceId = p })));

            CreateMap<Preference, PreferenceDto>();
        }
    }
}