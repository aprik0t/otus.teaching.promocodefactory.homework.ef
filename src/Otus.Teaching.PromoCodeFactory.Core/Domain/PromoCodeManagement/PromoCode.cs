﻿using System;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [Required]
        [StringLength(100)]
        public string Code { get; set; }

        [StringLength(100)]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        [Required]
        [StringLength(100)]
        public string PartnerEmail { get; set; }
        public Guid PartnerManagerId { get; set; }
        public virtual Employee Employee { get; set; }
        
        public Guid PreferenceId { get; set; }
        public virtual Preference Preference { get; set; }

        public Guid CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
    }
}