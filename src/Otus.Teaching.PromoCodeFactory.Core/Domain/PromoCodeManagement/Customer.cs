﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [StringLength(100)]
        public string FirstName { get; set; }
        
        [StringLength(100)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [StringLength(100)]
        public string Email { get; set; }
        
        public DateTime? BirthDay { get; set; }
        
        public virtual ICollection<PromoCode> PromoCodes { get; set; }
        
        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }
    }
}