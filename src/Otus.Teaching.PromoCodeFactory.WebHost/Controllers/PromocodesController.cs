﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Services.Dto;
using Otus.Teaching.PromoCodeFactory.Services.Request;
using Otus.Teaching.PromoCodeFactory.Services.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IPromoCodesService _promoCodesService;

        public PromocodesController(IPromoCodesService promoCodesService)
        {
            _promoCodesService = promoCodesService;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PromoCodeDto>>> GetPromocodesAsync()
        {
            return Ok(await _promoCodesService.LoadAsync());
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Preference) ||
                string.IsNullOrWhiteSpace(request.CustomerEmail) ||
                string.IsNullOrWhiteSpace(request.PartnerEmail) ||
                string.IsNullOrWhiteSpace(request.PromoCode))
                return BadRequest();
            
            await _promoCodesService.CreateAndGivePromocode(request);
            return Ok();
        }
    }
}